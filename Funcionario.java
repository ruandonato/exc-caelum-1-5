public class Funcionario{
	 String nome;
	 String departamento;
	 double salario;
	 String dataEntrada;
	 String rg;
	 double aumento;
	 double ganhoAnual;

public void recebeAumento(double umAumento){
	this.aumento = umAumento;
	salario = salario + aumento;

	}

public double calculaGanhoAnual(){
	ganhoAnual = salario * 12;
	return ganhoAnual;
	
	}
public void mostra(){
	System.out.println("Nome: " + this.nome);
	System.out.println("Departamento: " + this.departamento);
	System.out.println("Salario Atual: " + this.salario);
	System.out.println("Data de Entrada: " + this.dataEntrada);	
	System.out.println("RG: " + this.rg);		
	System.out.println("Ganho Anual: " + this.calculaGanhoAnual());	
	
	}
}
