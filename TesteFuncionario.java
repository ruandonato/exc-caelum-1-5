public class TesteFuncionario{

	public static void main(String [] args){
	//Instanciando primeiro Funcionario.	
	
	Funcionario umFuncionario = new Funcionario();
	umFuncionario.nome = "Ruan";
	umFuncionario.departamento = "Financeiro";
	umFuncionario.salario = 500;
	umFuncionario.dataEntrada = "12/09/92";
	umFuncionario.rg = "43211312";		
	umFuncionario.recebeAumento(100);
		
	umFuncionario.mostra();
		
	Funcionario outroFuncionario = new Funcionario();
	outroFuncionario = umFuncionario;
	
		if(umFuncionario == outroFuncionario){
		System.out.println("Funcionarios iguais");	
	
		}
	
		else{
		System.out.println("Funcionarios diferentes");

		}

	}

/*
4 - Com a criação de dois funcionarios instanciados de forma igual e sua comparação com o IF, constatamos que apesar de terem os mesmo atributos são funcionários diferentes.

5 - Igualando um Funcionario ao outro identificamos que eles tornam-se iguais pela condição IF.

*/
}
